# This is an Unfurl Cloud testbed project

This project manages shared cloud environments and deployments.

Unfurl Cloud commits this testbed's environments and deployments to the git repository in this project.

You can share resources publicly after they are deployed and control access through membership to this project.

Maintainers can access to the environments and the resources in it.

Other users that have access to this project can view and clone deployments into their own environments.

Maintainers can [clone this project locally](/home#clone-instructions) and keep all its settings and deployment data in sync as well deploy locally using the [unfurl](https://docs.unfurl.run) command-line.

Feel free to customize this README for your project.
